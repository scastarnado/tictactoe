# TicTacToe

Simple TicTacToe in Java

## Name
TicTacToe for Dummies.

## Description
This project has been created to practise and learn in a deeper way Java. It is a simple Tic Tac Toe game wich has to be played by two players. In future I can add a feature to configure if the game will be played by player vs machine or player vs player.

## Usage
It has been developed in Spanish, so all the in-game text is non-serious Spanish. If you like, I can add the English version.

## Support
You can help me in this project if you leave a comment telling what features can be great to add, to change, or delete. I appreciate all kind of constructive comments, despite those which are bad jokes.

## Roadmap
I will add all possible stuff which I find them useful and I am able to implement. All ideas are welcome.

## Contributing
I am open to get some help, or if any of you want to contribute to the project. If you feel encouraged enough, you can contact me through Discord (kastas#6486) and tell me who you are, and what would be great to do in this project.
Thanks to everyone though.

## Authors and acknowledgment
No one has contributed to make this project. It is a solo play. But I want to mention my Programming Subject professor, who put an exam, and I feel like I have to study. Thank you :)

## License
Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
https://creativecommons.org/licenses/by-sa/4.0/

## Project status
This project is in semi-final release. The main idea has been succesfully achieved, but I am open to make updates in order to improve the product/game.
