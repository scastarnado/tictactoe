import java.util.*;

public class TicTacToeMain {

	static Scanner a = new Scanner(System.in);
	static boolean partidaEnCurso = false;
	static jugador j1 = new jugador();
	static jugador j2 = new jugador();
	static coordenadas c = new coordenadas();


	public static void main(String[] args) {

		mostrarMenu();
		System.out.print("\nTu eleccion: ");
		int eleccion = a.nextInt();
		while (eleccion > 4 || eleccion < 1) {
			System.out.println("bobo o que escoge una opcion valida");
			System.out.print("Tu eleccion: ");
			eleccion = a.nextInt();
		}
		while (eleccion != 4) {
			switch (eleccion) {
			case 1:
				jugar();
				break;
			case 2:
				leadertablero();
				break;
			case 3:
				configuracion();
				break;
			default:
				System.out.println("Ha sido un placer");
			}
			mostrarMenu();
			eleccion = a.nextInt();
		}
		if (eleccion == 4) {
			System.out.println("Ha sido un placer");
		}
	}

	private static void jugar() {
		String tablero[][] = new String[][] { { " ", "1", " ", "2", " ", "3" }, { "1", " ", "|", " ", "|", " " },
			{ " ", "-", "-", "-", "-", "-" }, { "2", " ", "|", " ", "|", " " }, { " ", "-", "-", "-", "-", "-" },
			{ "3", " ", "|", " ", "|", " " } };
		while (j1.nombre == null || j2.nombre == null) {
			System.out.println();
			System.out.println(
					"Aun no has configurado ningun jugador.\nAccede al menu de Configuracion para crear a los jugadores.\n");
			configuracion();
		}
		System.out.println("EMPIEZA LA AVENTURA EN ESTE TREPIDANTE JUEGO!");
		partidaEnCurso = true;
		int turnoJugador = 1;
		imprimirMatriu(tablero);
		while (partidaEnCurso == true) {
			tirada(turnoJugador, tablero);
			if (turnoJugador == 1) {
				tablero[c.fila * 2 - 1][c.columna * 2 - 1] = "X";
				turnoJugador = 2;
			} else if (turnoJugador == 2) {
				tablero[c.fila * 2 - 1][c.columna * 2 - 1] = "O";
				turnoJugador = 1;
			}
			imprimirMatriu(tablero);
			if (haGanado(tablero) == true) {
				System.out.println("El juego ha terminado!");
				partidaEnCurso = false;
			}
			if (empate() == true) {
				System.out.println("El juego ha terminado\nEMPATE!");
			}
		}
	}

	private static boolean empate() {
		
		return false;
	}

	private static boolean haGanado(String[][] tablero) {
		// COMPROBAR CADA FILA, POR CADA JUGADOR
		for (int j = 1; j <= tablero.length; j = j + 2) {
			if (tablero[1][j].equals("X") && tablero[3][j].equals("X") && tablero[5][j].equals("X")) {
				j1.numWins++;
				return true;
			} else if (tablero[1][j].equals("O") && tablero[3][j].equals("O") && tablero[5][j].equals("O")) {
				j2.numWins++;
				return true;
			}
		}
		// COMPROBAR CADA COLUMNA, POR CADA JUGADOR
		for (int i = 1; i <= tablero.length; i = i + 2) {
			if (tablero[i][1].equals("X") && tablero[i][3].equals("X") && tablero[i][5].equals("X")) {
				j1.numWins++;
				return true;
			} else if (tablero[i][1].equals("O") && tablero[i][3].equals("O") && tablero[i][5].equals("O")) {
				j2.numWins++;
				return true;
			}
		}
		// COMPROBAR LA DIAGONAL RECTA E INVERTIDA PARA EL JUGADOR 'X'
		if(tablero[1][1].equals("X") && tablero[3][3].equals("X") && tablero[5][5].equals("X")) {
			j1.numWins++;
			return true;
		}
		if (tablero[5][1].equals("X") && tablero[3][3].equals("X") && tablero[1][5].equals("X")) {
			j1.numWins++;
			return true;
		}
		// COMPROBAR LA DIAGONAL RECTA E INVERTIDA PARA EL JUGADOR 'O'		
		if(tablero[1][1].equals("O") && tablero[3][3].equals("O") && tablero[5][5].equals("O")) {
			j2.numWins++;
			return true;
		}
		if (tablero[5][1].equals("O") && tablero[3][3].equals("O") && tablero[1][5].equals("O")) {
			j2.numWins++;
			return true;
		}
		return false;
	}

	private static void tirada(int turnoJugador, String tablero[][]) {
		if (turnoJugador == 1) {
			System.out.println("Turno del jugador " + j1.nombre);
		} else if (turnoJugador == 2) {
			System.out.println("Turno del jugador " + j2.nombre);
		}
		preguntarTirada();

		while (turnoJugador == 2 && tablero[c.fila * 2 - 1][c.columna * 2 - 1].equals("X")) {
			System.out.println("En esta casilla ya hay una pieza.");
			preguntarTirada();
		}
		while (turnoJugador == 1 && tablero[c.fila * 2 - 1][c.columna * 2 - 1].equals("O")) {
			System.out.println("En esta casilla ya hay una pieza.");
			preguntarTirada();
		}

	}

	private static void preguntarTirada() {
		System.out.println("Escribe la fila donde quieres jugar: ");
		c.fila = a.nextInt();
		while (casillaValida(c.fila) == false) {
			System.out.println("Vuelve a introducir una fila valida:");
			c.fila = a.nextInt();
		}
		System.out.println("Escribe la columna donde quieres jugar: ");
		c.columna = a.nextInt();
		while (casillaValida(c.columna) == false) {
			System.out.println("Vuelve a introducir una columna valida:");
			c.columna = a.nextInt();
		}
	}

	private static boolean casillaValida(int coordenada) {
		if (coordenada > 3 || coordenada < 1) {
			return false;
		}
		return true;
	}

	private static void leadertablero() {
		if (j1.nombre == null || j2.nombre == null) {
			System.out.println(
					"Aun no has configurado ningun jugador.\nAccede al menu de Configuracion para crear a los jugadores.\n");
		} else {
			System.out.println("Don/Do�a " + j1.nombre + " ha ganado " + j1.numWins + " veces!");
			System.out.println("Don/Do�a " + j2.nombre + " ha ganado " + j2.numWins + " veces!");
			System.out.println();
		}
	}

	private static void configuracion() {
		menuConfig();
	}

	private static void menuConfig() {
		System.out.println("   1  -> Ver configuracion");
		System.out.println("   2  -> Cambiar configuracion");
		int eleccion = a.nextInt();
		while (eleccion < 1 || eleccion > 2) {
			System.out.println("tu tas tonto o kitipasa? Escoge una opcion disponible");
			eleccion = a.nextInt();
		}
		switch (eleccion) {
		case 1:
			if (j1.nombre == null || j2.nombre == null) {
				System.out.println("Aun no hay jugadores configurados.\n");
				cambiarConfig();
			} else {
				verConfig();
			}
			break;
		case 2:
			cambiarConfig();
		}
	}

	private static void verConfig() {
		System.out.println("El nickname del primer jugador es: " + j1.nombre);
		System.out.println("El nickname del segundo jugador es: " + j2.nombre);
	}

	private static void cambiarConfig() {
		a.nextLine();
		System.out.println("Cual es el nickname del Jugaodor 1?");
		System.out.println("Nickname J1: ");
		j1.nombre = a.nextLine();
		System.out.println("Cual es el nickname del Jugaodor 2?");
		System.out.println("Nickname J2: ");
		j2.nombre = a.nextLine();
	}

	private static void mostrarMenu() {
		// TODO Auto-generated method stub
		System.out.println("|||||||||||||||||");
		System.out.println("|| TIC TAC TOE ||");
		System.out.println("|||||||||||||||||");
		System.out.println();
		System.out.println("  1  -> Jugar");
		System.out.println("  2  -> Leaderboard");
		System.out.println("  3  -> Configuracion");
		System.out.println("  4  -> Salir");
	}

	private static void imprimirMatriu(String[][] tablero) {
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[0].length; j++) {
				System.out.print(tablero[i][j] + " ");
			}
			System.out.println();
		}
	}

}
